from django.contrib import admin
from .models import Dokter, Pasien, Resep, Obat
# Register your models here.







class DokterAdmin(admin.ModelAdmin):
    list_display = ("nama", "nomor_telpon", "bidang", "jadwal_praktek")
    fileds = ("nama", "nomor_telpon", "bidang", "jadwal_praktek")

admin.site.register(Dokter, DokterAdmin)

class PasienAdmin(admin.ModelAdmin):
    list_display = ("nama", "nomor_telpon", "alamat", "keluhan")
    fileds = ("nama", "nomor_telpon", "alamat", "keluhan")

admin.site.register(Pasien, PasienAdmin)

class ResepAdmin(admin.ModelAdmin):
    list_display = ("nama", "total_harga", "Kumpulan_obat")
    fileds = ("nama", "total_harga", "Kumpulan_obat")

admin.site.register(Resep, ResepAdmin)


class ObatAdmin(admin.ModelAdmin):
    list_display = ("nama", "Kandungan", "Kasiat")
    fileds = ("nama", "Kandungan", "Kasiat")

admin.site.register(Obat, ObatAdmin)
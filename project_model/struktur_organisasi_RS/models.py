from django.db import models

# Create your models here.
class Dokter(models.Model):
    nama = models.CharField(max_length = 200)
    nomor_telpon = models.CharField(max_length = 200)
    bidang = models.CharField(max_length = 200)
    jadwal_praktek =  models.DateTimeField()

    def __str__(self):
        return self.nama


class Pasien(models.Model):
    nama = models.CharField(max_length = 200)
    nomor_telpon = models.CharField(max_length = 15)
    alamat = models.CharField(max_length = 200)
    keluhan = models.CharField(max_length = 1000)

class Resep(models.Model):
    nama = models.CharField(max_length = 200)
    total_harga = models.DecimalField(max_digits = 10, decimal_places=2)
    Kumpulan_obat = models.CharField(max_length = 500)


class Obat(models.Model):
    nama = models.CharField(max_length = 200)
    Kandungan = models.CharField(max_length = 200)
    Kasiat = models.CharField(max_length = 500)
